package com.cybersource.paypolis.game.engine.api.players

import org.springframework.stereotype.Service
import java.util.*

@Service
class PlayerRepository {
    private val players = mutableMapOf<UUID, Player>()

    fun getById(id: UUID): Player? {
        return players[id]
    }

    fun getByName(name: String): Player? {
        return players.entries.firstOrNull { it.value.name == name }?.value
    }

    fun getByUrl(url: String) :List<Player>{
        return players.entries.filter { it.value.url == url }?.map { it.value }
    }

    fun save(player: Player): Player {
        players[player.id] = player
        return player
    }

    fun getAll(): List<Player> {
        return players.values.toList()
    }


}