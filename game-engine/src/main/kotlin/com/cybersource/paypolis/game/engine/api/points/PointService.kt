package com.cybersource.paypolis.game.engine.api.points

import com.cybersource.paypolis.game.engine.api.events.Event
import com.cybersource.paypolis.game.engine.api.players.Player
import com.cybersource.paypolis.game.engine.api.events.EventService
import com.cybersource.paypolis.game.engine.model.Merchant
import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import com.cybersource.paypolis.gw.model.ResponseStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*
import javax.annotation.PreDestroy
import kotlin.collections.HashMap

@Service
class PointService(val eventService: EventService) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    // TODO change to atomic int
    private val playerPoints = HashMap<UUID, Int>()

    private val initialAmount = 100

    fun countPoints(errorTxPoints: Int, playerId: UUID, e: Throwable) {
        playerPoints[playerId] = playerPoints[playerId]!! - errorTxPoints
    }

    fun countPoints(approvedTxPoints: Int, errorTxPoints: Int, playerId: UUID, merchantId: UUID, paymentRequest: PaymentRequest, paymentResponse: PaymentResponse) {
        var pointsToUpdate = 0
        if (paymentResponse.status == ResponseStatus.ERROR) {
            pointsToUpdate -= errorTxPoints
        } else {
            pointsToUpdate = approvedTxPoints
        }

        playerPoints[playerId] = playerPoints[playerId]!! + pointsToUpdate
        eventService.putNewTransactionEvent(Event(playerId, pointsToUpdate, merchantId, paymentRequest, paymentResponse, null))

        if (playerPoints[playerId]!! < 0) {
            eventService.putNewTransactionEvent(Event(playerId, -playerPoints[playerId]!!, merchantId, paymentRequest, paymentResponse, "Feeling exhausted? Your points account decreased below 0, and the game master decided to balance it back to 0."))
            playerPoints[playerId] = 0
        }
    }

    // todo non thread safe
    fun payForMerchantIfPossible(merchant: Merchant, player: Player) {
        val points = playerPoints[player.id]
        val playerHasEnoughMoneyToBuyMerchant = points!! - merchant.costToBuyPoints > 0
        if (merchantIsForFree(merchant) || playerHasEnoughMoneyToBuyMerchant) {
            logger.info("Player ${player.id} pays ${merchant.costToBuyPoints} for ${merchant.displayName}")
            val newPoints = points - merchant.costToBuyPoints
            playerPoints[player.id] = newPoints
        } else {
            val message = "Player ${player.id} has no enough funds to buy ${merchant.displayName}"
            logger.warn(message)
            throw IllegalStateException(message)
        }
    }

    private fun merchantIsForFree(merchant: Merchant) =
            merchant.costToBuyPoints == 0

    fun register(player: Player) {
        if (playerPoints[player.id] == null) {
            logger.info("create points registry for player ${player.id}")
            playerPoints[player.id] = initialAmount
        }
    }

    @PreDestroy
    fun printResults() {
        playerPoints.forEach { println(it) }
    }

    fun getPoints(id: UUID) = playerPoints[id]

}