package com.cybersource.paypolis.game.engine.api.merchant

import com.cybersource.paypolis.game.engine.MerchantSpecs
import com.cybersource.paypolis.game.engine.api.players.PlayerService
import com.cybersource.paypolis.game.engine.api.players.PlayerSubscriptionService
import com.cybersource.paypolis.game.engine.api.points.PointService
import com.cybersource.paypolis.game.engine.model.Merchant
import com.cybersource.paypolis.game.engine.stream.TransactionStreamSubscriberManager
import org.springframework.web.bind.annotation.*
import java.lang.IllegalArgumentException
import java.util.*

@RestController
class MerchantController(val playerService: PlayerService, val playerSubscriptionService: PlayerSubscriptionService, val pointService: PointService) {

    @GetMapping("/merchants")
    fun getMerchants(): List<MerchantResponse> {
        return MerchantSpecs.merchants.map {
            MerchantResponse(it.displayName, it.costToBuyPoints, it.fieldNameId,
                    findAllSubscriptionsForMerchant(it))
        }
    }

    private fun findAllSubscriptionsForMerchant(merchant: Merchant) =
            TransactionStreamSubscriberManager.subscribers[merchant.id].orEmpty().map {
                PlayerResponse(
                        playerId = it.playerId.toString(),
                        playerName = playerService.getById(it.playerId)?.name ?: "unknown",
                        points = pointService.getPoints(it.playerId)!!,
                        merchants = playerSubscriptionService.merchants(it.playerId).map { it.displayName }
                )
            }

    @PostMapping("/merchants/{fieldName}")
    fun acquire(@PathVariable fieldName: String, @RequestParam playerId: UUID) {
        val merchant = MerchantSpecs.merchants.firstOrNull() { it.fieldNameId == fieldName }
                ?: throw IllegalArgumentException("Merchant Field ${fieldName} not found")
        val player = playerService.getById(playerId)!!
        playerSubscriptionService.playerBuysMerchant(merchant, player)
    }

    @DeleteMapping("/merchants/{fieldName}")
    fun release(@PathVariable fieldName: String, @RequestParam playerId: UUID) {
        val merchant = MerchantSpecs.merchants.first { it.fieldNameId == fieldName }
        val player = playerService.getById(playerId)!!
        playerSubscriptionService.playerDropsMerchant(merchant, player)
    }
}

data class MerchantResponse(val merchantName: String, val price: Int, val fieldName: String, val players: List<PlayerResponse>)
data class PlayerResponse(val playerId: String, val playerName: String, val points: Int, val merchants: List<String>)