package com.cybersource.paypolis.game.engine.api.events

import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import java.util.*

data class Event (val playerId: UUID,
                  val pointChange: Int,
                  val merchantId: UUID,
                  val paymentRequest: PaymentRequest,
                  val paymentResponse: PaymentResponse,
                  val description: String?)