package com.cybersource.paypolis.game.engine.api.players

import com.cybersource.paypolis.game.engine.MerchantSpecs
import com.cybersource.paypolis.game.engine.api.points.PointService
import com.cybersource.paypolis.game.engine.model.Merchant
import com.cybersource.paypolis.game.engine.stream.TransactionStreamSubscriberManager
import org.springframework.stereotype.Service
import java.util.*

@Service
class PlayerSubscriptionService(private val pointService: PointService) {

    fun playerBuysMerchant(merchant: Merchant, player: Player) {
        if (TransactionStreamSubscriberManager.isRegistered(playerId = player.id, merchantId = merchant.id)) {
            throw IllegalStateException("Player already subscribed to merchant")
        }
        pointService.payForMerchantIfPossible(merchant, player)
        val subscriber = PlayerTransactionStreamSubscriber(pointService, player, merchant)
        TransactionStreamSubscriberManager.registerSubscriber(merchant.id, subscriber)
    }

    fun playerDropsMerchant(merchant: Merchant, player: Player) {
        if (!TransactionStreamSubscriberManager.isRegistered(playerId = player.id, merchantId = merchant.id)) {
            throw IllegalStateException("Player is not subscribed to merchant")
        }
        TransactionStreamSubscriberManager.deregisterSubscriber(playerId = player.id, merchantId = merchant.id)
    }

    fun merchants(playerId: UUID) = TransactionStreamSubscriberManager.getSubscriptions(playerId).map { merchantId -> MerchantSpecs.merchants.first { it.id == merchantId } }
}