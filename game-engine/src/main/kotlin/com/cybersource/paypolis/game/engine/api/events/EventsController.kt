package com.cybersource.paypolis.game.engine.api.events

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors


@RestController
class EventsController(val eventsService: EventService) {

    @GetMapping("/events")
    fun getPlayerEvents(@RequestParam(required = false) playerId: UUID?) =
            eventsService.events
                    .stream()
                    .filter { event: Event -> playerId == null || event.playerId.equals(playerId) }
                    .sorted { o1, o2 -> o2.paymentRequest.transaction.transactionDateTime.compareTo(o1.paymentRequest.transaction.transactionDateTime) }
                    .limit(50)
                    .collect(Collectors.toUnmodifiableList())

}

