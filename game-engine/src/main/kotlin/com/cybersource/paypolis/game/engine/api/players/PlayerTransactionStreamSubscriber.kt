package com.cybersource.paypolis.game.engine.api.players

import com.cybersource.paypolis.game.engine.api.points.PointService
import com.cybersource.paypolis.game.engine.model.Merchant
import com.cybersource.paypolis.game.engine.model.PunishmentRules
import com.cybersource.paypolis.game.engine.stream.ATransactionStreamSubscriber
import com.cybersource.paypolis.game.engine.stream.TransactionStreamRequest
import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import com.cybersource.paypolis.gw.model.Transaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.ResourceAccessException
import org.springframework.web.client.RestTemplate
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.Flow

class PlayerTransactionStreamSubscriber(private val pointService: PointService, private val player: Player, private val merchant: Merchant) : ATransactionStreamSubscriber() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    override val merchantId: UUID = merchant.id
    override val playerId = player.id
    override val punishmentRules: PunishmentRules = merchant.transactionStreamSpecs.punishmentRules

    override fun onComplete() {
        logger.info("bye bye, I am not subscribing to transaction anymore :)")
    }

    override fun compareTo(other: ATransactionStreamSubscriber): Int {
        return if (playerId == other.playerId && merchantId == other.merchantId) 0 else 1
    }

    override fun onSubscribe(subscription: Flow.Subscription?) {
        pointService.register(player)
    }

    override fun onNext(item: TransactionStreamRequest?) {
        logger.info("Sending request to player ${player.name}")
        val paymentRequest = PaymentRequest(merchantTransactionId = UUID.randomUUID(), transaction = Transaction(ZonedDateTime.now(), BigDecimal(item!!.amount), item!!.currency))
        try {
            val postForEntity = RestTemplate().postForEntity(player.url, paymentRequest, PaymentResponse::class.java)
            pointService.countPoints(item.approvedTxPoints, item.failedTxPoints, player.id, merchantId, paymentRequest, postForEntity.body!!)
            super.onSuccessCallback()
        } catch (e: ResourceAccessException) {
            logger.warn("Error on player response ${e.message}")
            super.onMaxNoOfNoResponsesCallback()
            pointService.countPoints(item.failedTxPoints, player.id, e)
        } catch (e: HttpServerErrorException) {
            logger.warn("Error on player response ${e.message}")
            super.on500Callback()
            pointService.countPoints(item.failedTxPoints, player.id, e)
        } catch (e: Throwable) {
            logger.warn("Unexpected error on player response ${e.message}", e)
            super.on500Callback()
            pointService.countPoints(item.failedTxPoints, player.id, e)
        }

    }

    override fun onError(throwable: Throwable?) {
        TODO("Not yet implemented")
    }

}