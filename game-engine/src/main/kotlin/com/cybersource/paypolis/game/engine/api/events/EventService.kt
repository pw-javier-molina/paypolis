package com.cybersource.paypolis.game.engine.api.events

import org.apache.commons.collections4.queue.CircularFifoQueue
import org.springframework.stereotype.Service
import java.util.*

@Service
class EventService {
    val events: Queue<Event> = CircularFifoQueue<Event>(1000)

    // todo non thread safe
    fun putNewTransactionEvent(event: Event) {
        events.add(event)
    }
}