package com.cybersource.paypolis.game.engine.api.players

import com.cybersource.paypolis.game.engine.api.points.PointService
import com.cybersource.paypolis.game.engine.stream.TransactionStreamSubscriberManager
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException
import java.util.*

@Service
class PlayerService(val pointService: PointService, val playerRepository: PlayerRepository) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    fun getById(id: UUID): Player? {
        return playerRepository.getById(id)
    }

    fun getAll() : List<Player>{
        return playerRepository.getAll()
    }

    fun registerNewPlayer(name: String, url: String): Player {
        val playerByUrl = playerRepository.getByUrl(url)
        var player = playerRepository.getByName(name)

        if (player != null && playerByUrl.contains(player)) {
            logger.info("Player already registered, just return")
            return player!!
        }

        if (!playerByUrl.isEmpty()) {
            // TODO probably place for cheating ;)
            playerByUrl.forEach{
                logger.warn("URL ${url} is already registered for different user ${it.name} - unsubcribe this merchant and register new one")
                TransactionStreamSubscriberManager.deregisterAllSubscriberForPlayer(playerId = it.id)
            }
        }

        if (player == null) {
            logger.info("Register new player")
            player = playerRepository.save(Player(UUID.randomUUID(), name = name, url = url))
            pointService.register(player)
        } else {
            logger.info("Player already registered")
            if (url != player.url) {
                logger.info("Player changed the URL, unsubscribe all player's subscriptions")
                TransactionStreamSubscriberManager.deregisterAllSubscriberForPlayer(playerId = player.id)
            }
            // update url
            player = playerRepository.save(player.copy(url = url))
        }
        return player
    }

}