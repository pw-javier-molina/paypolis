package com.cybersource.paypolis.game.engine.api.players

import com.cybersource.paypolis.game.engine.api.merchant.PlayerResponse
import com.cybersource.paypolis.game.engine.api.points.PointService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class PlayerController(val playerService: PlayerService, val pointService: PointService, val playerSubscriptionService: PlayerSubscriptionService) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @PostMapping("/players")
    fun registerNewPlayer(@RequestParam name: String, @RequestParam url: String): UUID {
        logger.info("Registering ${name} at ${url}")
        val player = playerService.registerNewPlayer(name, url)
        logger.info("Player ${name} registered with id ${player.id}")
        return player.id
    }

    @GetMapping("/players")
    fun getPlayers() = playerService
            .getAll()
            .map { PlayerResponse(it.id.toString(), it.name, pointService.getPoints(it.id)!!, playerSubscriptionService.merchants(it.id).map { it.displayName }) }

}