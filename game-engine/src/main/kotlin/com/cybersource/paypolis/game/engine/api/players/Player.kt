package com.cybersource.paypolis.game.engine.api.players

import java.util.*

data class Player(
        val id: UUID,
        val name: String,
        val url: String
)