package com.cybersource.paypolis.game.engine.model

data class Gateway(
        val name: String
)