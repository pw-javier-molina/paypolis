package com.cybersource.paypolis.gw.model

import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*

data class PaymentRequest(
        val merchantTransactionId: UUID,
        val transaction: Transaction
//        val card: CardData?,
//        val pos: PosData?,
//        val bankName: String?
)

data class Transaction(
        val transactionDateTime: ZonedDateTime,
        val amount: BigDecimal,
        val currency: Currency
)

data class CardData(
        val cardData: DuktpData,
        val pinData: DuktpData? = null
)

data class PosData(
        val entryMode: EntryMode,
        val cvm: Cvm,
        val icc: String? = null
)


enum class Cvm {
    NONE, SIGNATURE, PIN, CDVCM
}

enum class EntryMode {
    MANUAL, MAGSTRIPE, ICC, NFC
}

data class DuktpData(val data: String, val ksn: String)