package com.cybersource.paypolis.gw.model

data class PaymentResponse(
        val status: ResponseStatus,
        val authCode: String
)

enum class ResponseStatus {
    ERROR, APPROVED, DECLINED
}
