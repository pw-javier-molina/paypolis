package com.cybersource.paypolis.gw.model.payment

import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/payment")
class PaymentController(val paymentService: PaymentService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @PostMapping
    fun processTransaction(@RequestBody paymentRequest: PaymentRequest): PaymentResponse {
        logger.info("Bank received the request => ${paymentRequest}")
        return paymentService.performPayment(paymentRequest)
    }

}