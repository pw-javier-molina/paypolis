package com.cybersource.paypolis.gw.model.payment

import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import com.cybersource.paypolis.gw.model.ResponseStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit
import kotlin.random.Random

@Service
class PaymentService() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    fun performPayment(paymentRequest: PaymentRequest): PaymentResponse {
        if (paymentRequest.transaction.amount < BigDecimal.ZERO || paymentRequest.transaction.transactionDateTime > ZonedDateTime.now()) {
            return PaymentResponse(ResponseStatus.valueOf("400"), "Malformed request")
        }

        randomSleep()
        randomException()

        return PaymentResponse(ResponseStatus.APPROVED, "OK")
    }

    private fun randomException() {
        if (Random.nextInt() % 20 == 0) {
            throw IllegalStateException("random exception")
        }
    }

    private fun randomSleep() {
        val millis = Random.nextLong(20) * 100
        logger.info("sleep for ${millis}ms")
        TimeUnit.MILLISECONDS.sleep(millis)
    }
}