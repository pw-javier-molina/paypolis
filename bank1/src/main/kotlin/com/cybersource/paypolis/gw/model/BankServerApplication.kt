package com.cybersource.paypolis.gw.model

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BankServerApplication

fun main(args: Array<String>) {
    runApplication<BankServerApplication>(*args)
}

