### Swagger for Game Engine
http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

### how to compile docker images locally?
`mvn compile package jib:dockerBuild -pl player-sample-app,game-engine,bank1 --also-make`

In case you have problems (for example Visa certificate) you can use `-Djib.allowInsecureRegistries=true`

### How to scale app clients
`/usr/local/bin/docker-compose up --scale app1=3`

### how to install angular ? 
```
brew install npm
npm install @angular/cli
npm install --save-dev @angular-devkit/build-angular
```
How to run?
```
cd frontend-app
ng serve --open
```