package com.cybersource.paypolis.gw.model.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "player")
class AppProperties {
    var name: String? = null
    lateinit var names: List<String>

    lateinit var selfHost: String
    lateinit var selfPort: String

    lateinit var gameHost: String
    lateinit var gamePort: String

    lateinit var bank1Host: String
    lateinit var bank1Port: String

    var autoBuyMerchants: Boolean = true
}