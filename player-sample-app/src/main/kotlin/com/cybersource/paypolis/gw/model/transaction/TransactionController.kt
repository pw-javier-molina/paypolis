package com.cybersource.paypolis.gw.model.transaction

import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("/tx")
class TransactionController(val transactionClient: TransactionClient) {

    @PostMapping
    fun processTransaction(@RequestBody paymentRequest: PaymentRequest): PaymentResponse {
        println("We are processing something!! YaY!")
        return transactionClient.forwardTransaction(paymentRequest)
    }

}