package com.cybersource.paypolis.gw.model.transaction

import com.cybersource.paypolis.gw.model.PaymentRequest
import com.cybersource.paypolis.gw.model.PaymentResponse
import com.cybersource.paypolis.gw.model.config.AppProperties
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForEntity

@Service
class TransactionClient(val appProperties: AppProperties, val restTemplate: RestTemplate) {

    val bankUrl = "http://${appProperties.bank1Host}:${appProperties.bank1Port}/payment"

    fun forwardTransaction(paymentRequest: PaymentRequest): PaymentResponse {
        return restTemplate.postForEntity<PaymentResponse?>(bankUrl, paymentRequest, PaymentResponse::class.java).body!!
    }
}