package com.cybersource.paypolis.gw.model

import com.cybersource.paypolis.gw.model.config.AppProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForEntity
import java.lang.Thread.sleep
import java.util.*
import javax.annotation.PostConstruct
import kotlin.random.Random

@Service
class GameController(val appProperties: AppProperties,
                     val restTemplate: RestTemplate) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @PostConstruct
    fun registerGame() {
        waitForGameToBeUp()
        val playerId = registerPlayer()

        var registeredFieldsInRandomOrder: Queue<String> = LinkedList((1..8).map { "field${it.toString().padStart(2, '0')}" }.shuffled())

        if (appProperties.autoBuyMerchants) {
            val nextInt = Random.nextInt(5) + 1
            logger.info("Register to ${nextInt} merchants")
            for (i in 0..nextInt) {
                val field = registeredFieldsInRandomOrder.poll()
                logger.info("Register for field ${field}")
                registerMerchantWithField(playerId, field)
            }
        } else {
            logger.info("Do not buy merchants at start")
        }
    }

    private fun waitForGameToBeUp() {
        for (x in 1..100) {
            logger.info("wait for the game service attempt ${x}")
            try {
                val url = "http://${appProperties.gameHost}:${appProperties.gamePort}/actuator/health"
                val forEntity = restTemplate.getForEntity(url, String::class.java)
                if (forEntity.statusCode.is2xxSuccessful) {
                    break
                }
            } catch (e: Exception) {
                logger.warn("game not ready ${e.message}")
                sleep(1000)
            }
        }
    }


    private fun registerMerchantWithField(playerId: UUID, field: String) {
        val url = "http://${appProperties.gameHost}:${appProperties.gamePort}/merchants/$field?playerId=${playerId}"
        restTemplate.postForEntity<String?>(url, null, String::class.java)
        logger.info("registered")
    }

    private fun registerPlayer(): UUID {
        val teamName = getTeamName()
        val myUrlParam = "http://${appProperties.selfHost}:${appProperties.selfPort}/tx"
        val url = "http://${appProperties.gameHost}:${appProperties.gamePort}/players?name=$teamName&url=$myUrlParam"
        val playerId = restTemplate.postForEntity<UUID?>(url, null, UUID::class.java)
        logger.info("player id ${playerId}")
        return playerId.body!!
    }

    private fun getTeamName(): String =
            if (appProperties.name.isNullOrEmpty()) {
                "\"${appProperties.name}\""
            } else {
                logger.info("Use random generated name")
                "\"${appProperties.names.shuffled().take(1)[0]}\""
            }

}