package com.cybersource.paypolis.gw.model

import com.cybersource.paypolis.gw.model.config.AppProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
@EnableConfigurationProperties(AppProperties::class)
class GwServerApplication

fun main(args: Array<String>) {
    runApplication<GwServerApplication>(*args)
}

