package com.cybersource.paypolis.game.engine.stream

import com.cybersource.paypolis.game.engine.model.PunishmentRules
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.Flow

interface TransactionStreamSubscriber : Flow.Subscriber<TransactionStreamRequest> {
    val playerId: UUID
    val merchantId: UUID
    val punishmentRules: PunishmentRules
}

abstract class ATransactionStreamSubscriber : TransactionStreamSubscriber, Comparable<ATransactionStreamSubscriber> {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    val errorsCounter = ErrorsCounter(0, 0, 0)

    fun onSuccessCallback() {
        errorsCounter.reset()
    }

    fun on500Callback() {
        errorsCounter.maxNoOf500Responses++
        checkPunishmentRules()
    }

    fun onTimeoutCallback() {
        errorsCounter.maxNoOf500Responses++
        checkPunishmentRules()
    }

    fun onMaxNoOfNoResponsesCallback() {
        errorsCounter.maxNoOf500Responses++
        checkPunishmentRules()
    }

    private fun checkPunishmentRules() {
        if (errorsCounter.maxNoOf500Responses > punishmentRules.maxNoOf500Responses
                || errorsCounter.maxNoOfNoResponses > punishmentRules.maxNoOfNoResponses
                || errorsCounter.maxNoOfTimeouts > punishmentRules.maxNoOfTimeouts) {
            logger.info("Unsubscribe myself because of too many errors ${errorsCounter}")
            TransactionStreamSubscriberManager.deregisterSubscriber(this)
        }
    }

    // todo not ideal thread safe ;)
    data class ErrorsCounter(var maxNoOf500Responses: Int,
                             var maxNoOfTimeouts: Int,
                             var maxNoOfNoResponses: Int) {

        fun reset() {
            maxNoOf500Responses = 0
            maxNoOfNoResponses = 0
            maxNoOfTimeouts = 0
        }
    }
}