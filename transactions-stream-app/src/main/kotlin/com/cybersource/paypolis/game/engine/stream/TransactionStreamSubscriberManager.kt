package com.cybersource.paypolis.game.engine.stream

import com.cybersource.paypolis.game.engine.model.Merchant
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentSkipListSet
import kotlin.concurrent.thread
import kotlin.streams.toList

object TransactionStreamSubscriberManager {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    val subscribers: ConcurrentHashMap<UUID, ConcurrentSkipListSet<TransactionStreamSubscriber>> = ConcurrentHashMap()

    fun registerSubscriber(merchantId: UUID, subscriber: TransactionStreamSubscriber) {
        if (subscribers[merchantId] == null) {
            subscribers[merchantId] = ConcurrentSkipListSet()
        }
        subscribers[merchantId]?.add(subscriber)
        subscriber.onSubscribe(null)
    }

    fun isRegistered(playerId: UUID, merchantId: UUID): Boolean {
        return subscribers[merchantId]?.filter { it.playerId == playerId }?.isNotEmpty() ?: false
    }

    fun deregisterAllSubscriberForPlayer(playerId: UUID) {
        subscribers.entries
                ?.map { it.value.stream() }
                .flatMap { it.toList() }
                .filter { it.playerId == playerId }
                .forEach { deregisterSubscriber(it) }
    }

    fun deregisterSubscriber(playerId: UUID, merchantId: UUID) {
        val subscriber = subscribers[merchantId]?.first { it.playerId == playerId }
        if (subscriber != null) {
            deregisterSubscriber(subscriber)
        }
    }

    fun deregisterSubscriber(subscriber: TransactionStreamSubscriber) {
        subscribers[subscriber.merchantId]?.remove(subscriber)
    }

    fun publishToAllSubscribers(merchant: Merchant, transactionStreamRequest: TransactionStreamRequest) {
        subscribers[merchant.id]?.iterator()?.forEach {
            thread(start = true, isDaemon = true, name = "transactionStreamForMerchant${merchant.id}${it.playerId}") {
                it.onNext(transactionStreamRequest)
            }
        }
    }

    fun getSubscriptions(player: UUID) = subscribers.filter { it.value.any { it.playerId == player } }.map { it.key }.toList()
}