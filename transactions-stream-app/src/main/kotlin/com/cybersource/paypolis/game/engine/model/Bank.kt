package com.cybersource.paypolis.game.engine.model

enum class Bank {
    BANK1, BANK2, BANK3
}