package com.cybersource.paypolis.game.engine

import com.cybersource.paypolis.game.engine.model.Merchant
import java.util.*

interface MerchantRepository {
    fun findAll(): List<Merchant> {
        return MerchantSpecs.merchants
    }

    fun findById(id: UUID): Merchant {
        return MerchantSpecs.merchants.first { it.id == id }
    }

}