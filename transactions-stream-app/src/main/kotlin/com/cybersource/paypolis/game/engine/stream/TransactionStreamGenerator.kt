package com.cybersource.paypolis.game.engine.stream

import com.cybersource.paypolis.game.engine.MerchantSpecs
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class TransactionStreamGenerator {

    @PostConstruct
    fun initializeMerchants() {
        println("Initializing Merchants")
        MerchantSpecs.merchants.forEach {
            TransactionStreamPublisher(it).start()
        }
    }
}