package com.cybersource.paypolis.game.engine

import com.cybersource.paypolis.game.engine.model.Bank
import com.cybersource.paypolis.game.engine.model.Merchant
import com.cybersource.paypolis.game.engine.model.TransactionStreamSpecs

object MerchantSpecs {

    val merchants = listOf(
            Merchant(fieldNameId = "field01", displayName = "Old Kent Road", costToBuyPoints = 20, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 10, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 10, destinationBank = emptyList())),
            Merchant(fieldNameId = "field02", displayName = "Whitechapel Road", costToBuyPoints = 50, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 100, pointsPerSuccessfulTx = 20, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
            Merchant(fieldNameId = "field03", displayName = "King's Cross station", costToBuyPoints = 90, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 50, maxTxAmount = 100, pointsPerSuccessfulTx = 30, penaltyPointsPerInvalidTx = 2, destinationBank = emptyList())),
            Merchant(fieldNameId = "field04", displayName = "The Angel, Islington", costToBuyPoints = 200, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 100, maxTxAmount = 500, pointsPerSuccessfulTx = 50, penaltyPointsPerInvalidTx = 10, destinationBank = emptyList())),
            Merchant(fieldNameId = "field05", displayName = "Euston Road", costToBuyPoints = 500, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 3, minTxAmount = 1, maxTxAmount = 200, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
            Merchant(fieldNameId = "field06", displayName = "Pentonville Road", costToBuyPoints = 2000, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
            Merchant(fieldNameId = "field07", displayName = "Pall Mall", costToBuyPoints = 5000, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 10, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
            Merchant(fieldNameId = "field08", displayName = "Whitehall", costToBuyPoints = 100000, transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 10, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList()))
//            Merchant(fieldNameId = "field09", displayName = "Northumberland Avenue", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 100, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field10", displayName = "Marylebone station", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 5, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field11", displayName = "Bow Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field12", displayName = "Great Marlborough Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 33, penaltyPointsPerInvalidTx = 11, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field13", displayName = "Vine Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field14", displayName = "Strand", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field15", displayName = "Fleet Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 99, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field16", displayName = "Trafalgar Square", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 200, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field17", displayName = "Fenchurch Street station", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field18", displayName = "Leicester Square", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field19", displayName = "Coventry Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field20", displayName = "Piccadilly", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 20, penaltyPointsPerInvalidTx = 4, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field21", displayName = "Regent Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 5, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field22", displayName = "Oxford Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 5, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field23", displayName = "Bond Street", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 10, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field24", displayName = "Liverpool Street station", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 1, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field25", displayName = "Park Lane", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 1, maxTxAmount = 1, pointsPerSuccessfulTx = 22, penaltyPointsPerInvalidTx = 1, destinationBank = emptyList())),
//            Merchant(fieldNameId = "field26", displayName = "Mayfair", transactionStreamSpecs = TransactionStreamSpecs(txPerSecond = 1, minTxAmount = 10, maxTxAmount = 50, pointsPerSuccessfulTx = 10, penaltyPointsPerInvalidTx = 60, destinationBank = listOf(Bank.BANK1)))
    )
}


