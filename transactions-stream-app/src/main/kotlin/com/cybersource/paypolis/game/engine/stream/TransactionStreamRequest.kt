package com.cybersource.paypolis.game.engine.stream

import com.cybersource.paypolis.game.engine.model.Bank
import java.util.*

data class TransactionStreamRequest(val amount: Int, val requiredBank: List<Bank>, val currency: Currency, val approvedTxPoints: Int, val failedTxPoints: Int)