package com.cybersource.paypolis.game.engine.model

data class TransactionStreamSpecs(
        val txPerSecond: Int,

        val minTxAmount: Int, // 5
        val maxTxAmount: Int, // 10

        val pointsPerSuccessfulTx: Int,
        val penaltyPointsPerInvalidTx: Int,

        val punishmentRules: PunishmentRules = PunishmentRules(20, 20, 20),

        val destinationBank: List<Bank>
// Specify the amount of purchasing this super cool merchant
        // FUTURE if we want to add DCC to the engine
//        val supportedTxCurrency: List<String>,
//        val bankSupportedTxCurrency: List<String>
)

data class PunishmentRules(
        val maxNoOf500Responses: Int,
        val maxNoOfTimeouts: Int,
        val maxNoOfNoResponses: Int
)
