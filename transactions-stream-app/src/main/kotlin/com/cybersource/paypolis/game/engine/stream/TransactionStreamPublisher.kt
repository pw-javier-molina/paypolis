package com.cybersource.paypolis.game.engine.stream

import com.cybersource.paypolis.game.engine.model.Merchant
import java.time.Duration
import java.util.*

class TransactionStreamPublisher(
        val merchant: Merchant
) {

    fun start() {
        println("Running scheduler")
        Timer().scheduleAtFixedRate(TransactionStreamJob(merchant), 0, frequency())
    }

    private fun frequency() =
            Duration.ofSeconds(1).dividedBy(merchant.transactionStreamSpecs.txPerSecond.toLong()).toMillis()


}