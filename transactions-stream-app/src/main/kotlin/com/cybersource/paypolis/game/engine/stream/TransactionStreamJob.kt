package com.cybersource.paypolis.game.engine.stream

import com.cybersource.paypolis.game.engine.model.Merchant
import com.cybersource.paypolis.game.engine.model.TransactionStreamSpecs
import java.util.*

class TransactionStreamJob(val merchant: Merchant) : TimerTask() {

    override fun run() {
        sendTransactionToStreamApiGate(generateTransaction(merchant.transactionStreamSpecs))
    }

    private fun sendTransactionToStreamApiGate(transaction: TransactionStreamRequest) {
        TransactionStreamSubscriberManager.publishToAllSubscribers(merchant, transaction)
    }

    private fun generateTransaction(specs: TransactionStreamSpecs): TransactionStreamRequest {
        val amount = Random().nextInt(Math.max(specs.maxTxAmount - specs.minTxAmount, 1)) + specs.minTxAmount
        return TransactionStreamRequest(amount, specs.destinationBank, Currency.getInstance("EUR"), specs.pointsPerSuccessfulTx, specs.penaltyPointsPerInvalidTx)
    }
}