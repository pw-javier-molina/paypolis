package com.cybersource.paypolis.game.engine.model

import java.util.*

data class Merchant(
        val fieldNameId: String, // used to identify on the UI map
        val displayName: String, // human friendy to display
        val transactionStreamSpecs: TransactionStreamSpecs,
        val costToBuyPoints: Int = 10
) {
    val id: UUID = UUID.randomUUID() // internal UUID
}