package com.cybersource.paypolis.bank1

import com.cybersource.paypolis.bank.`interface`.Bank
import com.cybersource.paypolis.bank.model.BankRequest
import com.cybersource.paypolis.bank.model.BankResponse
import com.cybersource.paypolis.bank.model.BankResponseStatus

class Bank1 : Bank {
    override fun processTransaction(request: BankRequest): BankResponse {
        return BankResponse("The auth id", BankResponseStatus.ERROR)
    }
}