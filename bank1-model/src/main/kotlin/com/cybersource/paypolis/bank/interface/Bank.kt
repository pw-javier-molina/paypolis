package com.cybersource.paypolis.bank.`interface`

import com.cybersource.paypolis.bank.model.BankRequest
import com.cybersource.paypolis.bank.model.BankResponse

interface Bank {

    fun processTransaction(request: BankRequest): BankResponse
}