package com.cybersource.paypolis.bank.model

data class BankResponse(
        val authId: String,
        val status: BankResponseStatus
)

enum class BankResponseStatus {
    APPROVED, DECLINED, ERROR
}