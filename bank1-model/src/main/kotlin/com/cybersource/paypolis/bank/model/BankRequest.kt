package com.cybersource.paypolis.bank.model

import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*

data class BankRequest(
        val card: Card,
        val amount: BigDecimal,
        val currency: Currency,
        val transactionDateTime: ZonedDateTime,
        val merchantId: String,
        val terminalId: String,
        val rrn: String
)

data class Card(
        val pan: String,
        val expiryDate: String
)