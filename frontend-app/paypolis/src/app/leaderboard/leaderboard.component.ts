import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Merchant} from "../models/merchant";
import {ToastrService} from "ngx-toastr";
import {Player} from "../models/player";
import {MatDialog} from '@angular/material/dialog';
import {DialogOverview} from "./dialogOverview/dialog-overview.component";
import {interval} from "rxjs";

export interface DialogData {
  merchant: Merchant;
  players: Array<Player>;
  buyingPlayerId: String;
}

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {
  appUrl = "http://localhost:4200/api";
  merchants: Array<Merchant>;
  players: Array<Player>;
  oldPlayers = new Array<Player>();
  loading = true;
  buyingPlayerId: String;
  private dialogOpen: boolean = false;

  constructor(private httpClient: HttpClient, private toastr: ToastrService, public dialog: MatDialog) {
  }

  myFunction(player) {
    return player.playerName;
  }

  ngOnInit(): void {
    this.getMerchantAndPlayers();
    interval(1000).subscribe(() => this.getMerchantAndPlayers());
    interval(1000).subscribe(() => this.oldPlayers = this.players)
  }

  getMerchantAndPlayers() {
    this.httpClient.get(this.appUrl + "/merchants").subscribe((data: Array<Merchant>) => {
        this.loading = true;
        this.merchants = data;
      }, () => this.toastr.error("Couldn't fetch merchants")
      , () => this.loading = false);


    this.httpClient.get(this.appUrl + "/players").subscribe((data: Array<Player>) => {
        this.loading = true;
        this.players = data;
      }, () => this.toastr.error("Couldn't fetch players")
      , () => this.loading = false)
  }

  openDialog(merchant: Merchant) {
    this.dialogOpen = true;
    const dialogRef = this.dialog.open(DialogOverview, {
      width: '250px',
      data: {merchant, players: this.players}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.buyMerchant(merchant.fieldName, result);
      console.log('The dialog was closed');
      console.log(result);
      this.buyingPlayerId = result;
    }, () => {
    }, () => {
      this.dialogOpen = false;
    });
  }

  info() {
    this.toastr.info("Paypolis - authors: Pawel O. Pawel W. Maciej U. Javier M A. Mateusz B")
  }

  buyMerchant(fieldName: String, playerId: String) {
    this.httpClient.post(this.appUrl + "/merchants/" + fieldName + "?playerId=" + playerId, null).subscribe(() => {
      this.toastr.success("Successfully bought merchant!");
      },
      () => this.toastr.error("Couldn't buy merchant " + fieldName))
  }

  buyBrown() {
    this.openDialog({merchantName: "Old Kent Road", fieldName: "field01", costToBuyPoints: 20, players: []});
  }

  buyBlue() {
    this.openDialog({merchantName: "Whitechapel Road", fieldName: "field02", costToBuyPoints: 50, players: []});
  }

  buyPink() {
    this.openDialog({merchantName: "King Cross station", fieldName: "field03", costToBuyPoints: 90, players: []});
  }

  buyOrange() {
    this.openDialog({merchantName: "The Angel, Islington", fieldName: "field04", costToBuyPoints: 200, players: []});
  }

  buyRed() {
    this.openDialog({merchantName: "Euston Road", fieldName: "field05", costToBuyPoints: 500, players: []});
  }

  buyYellow() {
    this.openDialog({merchantName: "Pentonville Road", fieldName: "field06", costToBuyPoints: 2000, players: []});
  }

  buyGreen() {
    this.openDialog({merchantName: "Pall Mall", fieldName: "field07", costToBuyPoints: 5000, players: []});
  }

  buyDarkBlue() {
    this.openDialog({merchantName: "Whiteall", fieldName: "field08", costToBuyPoints: 100000, players: []});
  }

  applyStyle(player: Player): String {
    let oldPlayer = this.oldPlayers.filter(p => p.playerId == player.playerId)[0];
    if (player && oldPlayer) {
      if (player.points > oldPlayer.points) {
        return "alert-success"
      } else if (player.points < oldPlayer.points) {
        return "alert-danger"
      } else {
      }
    } else {
      return "";
    }
  }
}
