import {Player} from "./player";

export class Merchant {
  merchantName: String;
  costToBuyPoints: number;
  fieldName: String;
  players: Array<Player>;
}
