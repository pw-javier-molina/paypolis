export class Player {
  playerId: String;
  playerName: String;
  points: number;
  merchants: Array<String>
}
