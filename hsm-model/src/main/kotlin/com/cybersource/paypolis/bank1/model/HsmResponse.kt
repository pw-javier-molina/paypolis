package com.cybersource.paypolis.bank1.model

data class Bank1Response(
        val authId: String,
        val status: Bank1ResponseStatus
)

enum class Bank1ResponseStatus {
    APPROVED, DECLINED, ERROR
}