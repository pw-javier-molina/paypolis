package com.cybersource.paypolis.bank1.model

import java.util.*

data class HsmResponse(
        val tokenId: UUID,
        val pan: String,
        val track2Data: String
)

